from distutils.core import setup

setup(
    name='CeleryMongoBroker',
    version='0.1.2',
    description='Updated celery/kombu broker that uses MongoDB',
    author='Arlena Derksen',
    author_email='arlena@hubsec.eu',
    classifiers=[
            'Development Status :: 4 - Beta',
            'Intended Audience :: Developers',
            'Programming Language :: Python :: 3.6'
    ],
    packages=['celery_mongo_broker'],
    install_requires=['vine', 'kombu>=4.0.2,<5.0', 'pymongo>=3.4.0']
)
